Homotopy type theory jumpstart script.

This script downloads Vladimir Voevodsky's code and coq 8.3. It then patches
and builds coq; ready to hack!

Usage:

 git clone https://bitbucket.org/wires/htt
 cd htt
 sh BUILDSCRIPT

(The script requires curl)
